const button = document.getElementById('button')

button.addEventListener('click', game)

let corpo = document.createElement("div") 
corpo.id='main' 

let modal = document.getElementById("myModal");

button.addEventListener('click', function(){
    modal.style.display = "none";
    document.body.appendChild(corpo)
})

let board = [
    [0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0], 
    [0, 0, 0, 0, 0, 0],  
    [0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0]
]

function game(){

const edgeX = board[0].length-1;
const edgeY = board.length-3;

    let vez = document.getElementById('vez')
    vez.innerText = 'Vez do jogador: '


    let discoAtual = document.createElement('div')
    discoAtual.id = 'discoAtual'
    document.getElementById('vez').appendChild(discoAtual)
 
    
let resultado=false;    


function hidden(){
    let reset= document.createElement('button')
    reset.id="reset";
    reset.innerText="Reiniciar"
    reset.className="button"
    reset.style.marginRight="300px"
    reset.style.marginTop="10px"
    reset.onclick=function(){
        location.reload();
    }
    corpo.innerText="";
    let mensagem=document.getElementById('conteudo');
    if(resultado=="empate"){
        mensagem.innerText="EMPATOU"
        mensagem.style.textAlign="center"
        mensagem.appendChild(reset)
    }else if(resultado==true){
        mensagem.innerText="VOCÊ VENCEU!" + jogadorAtual;
        if(jogadorAtual==='player2'){
            mensagem.innerText="Você venceu Jogador 1 (Bola preta).";
            mensagem.style.textAlign="center"
            mensagem.appendChild(reset)
        }else{
            mensagem.innerText="Você venceu Jogador 2 (Bola cinza).";
            mensagem.style.textAlign="center"
            mensagem.appendChild(reset)
        }
    }
    corpo.appendChild(mensagem);
}


for(i=0;i<board.length;i++){    
    let newLine = document.createElement("div")
    newLine.className="coluna "+i
    newLine.id="coluna"+i
    corpo.appendChild(newLine)
       

    for(j=0;j<board[i].length;j++){ 
        let newSpace = document.createElement("div")
            
        newSpace.className=board[i][j]
        newSpace.id="celula"+i+j;
        newLine.appendChild(newSpace)
    }
    let index=document.createElement('div')
    index.id="index"+i;
    index.className=0;
    
    newLine.appendChild(index)

}

let jogadorAtual = "player1"

    let mostrador = document.getElementById("discoAtual")
    let exibidor = document.createElement("div")
    mostrador.appendChild(exibidor)
    
    if(jogadorAtual === "player1"){
        exibidor.className="p"
        
    }
    else if(jogadorAtual ==="player2"){
        exibidor.className="v"
    }

    corpo.addEventListener("mouseover",(mouseOver)=>{

        if(mouseOver.target.className.charAt(0)=="c") {
            if(jogadorAtual === "player1"){
                mouseOver.target.lastElementChild.className="p";
            }
            else if(jogadorAtual ==="player2"){
                mouseOver.target.lastElementChild.className="v";
            }
        }else if(mouseOver.target.id.slice(7)=="5"){
            if(jogadorAtual === "player1"){
                mouseOver.target.parentElement.lastElementChild.className="p";
            }
            else if(jogadorAtual ==="player2"){
                mouseOver.target.parentElement.lastElementChild.className="v";
            }
        }

    })

    corpo.addEventListener("mouseout",(mouseOut)=>{
        if(mouseOut.target.className.charAt(0)=="c") {
            mouseOut.target.lastElementChild.className="0";
        }else if(mouseOut.target.id.charAt(0)=="i"){
            mouseOut.target.parentElement.lastElementChild.className="0";
        }
    })


    corpo.addEventListener("click",(event)=>{ 

        if(resultado==false){
            if(event.target.parentElement.id != "main" && event.target.id != "main" && event.target.id != "conteudo"){
                const coluna=event.target.parentElement.className;
                let celula1=event.target.parentElement.firstElementChild;
                
                
                let col=coluna.charAt(7);
                for(i=0;i<=col;i++){
                    if(i==col){
                        for(c=0;c<board[i].length;c++){
                            
                            if(board[i][c]==0 ){
                                for(cc=0;cc<c;cc++){
                                    celula1=celula1.nextSibling;
                                }
                                board[i].splice(c,1,"p");
                                    if(jogadorAtual === "player1"){
                                        board[i].splice(c,1,"p")
                                        celula1.className="p";
                                        jogadorAtual = "player2"
                                        exibidor.className="v"
                                    }
                                    else if(jogadorAtual === "player2"){
                                        board[i].splice(c,1,"v")
                                        celula1.className="v";
                                        jogadorAtual = "player1"
                                        exibidor.className="p"
                                    }
                                break;
                            }
                        }
            
                    }
                
                }  
            } 
        }
        
    resultado=vitoria();
        
    
        if( board[0][5] !=0 &&
            board[1][5] !=0 && 
            board[2][5] !=0 && 
            board[3][5] !=0 && 
            board[4][5] !=0 && 
            board[5][5] !=0 && 
            board[6][5] !=0 && 
            resultado == false){
            resultado="empate"
        }
    

    if (resultado==true && event.target.id != "conteudo" && event.target.id != "main"){
        hidden();        
    }else if(resultado=="empate"){
        hidden();
    }
    })

function vitoria(){

let vit=false;


//====== VERTICAL ====================================================================
for(let y = 0; y < board.length; y++){

  for(let x = 0; x < edgeX; x++) {
    let cell = board[y][x];
    
    if (cell !== 0 && cell ==="p") {
      
      if(cell === board[y][x+1] && cell === board[y][x+2] && cell === board[y][x+3] ) {
        console.log("Lig 4 VERTICAL PRETA " + (x+1) + ":" + (y+1) + "!");
        
        vit=true;
      }
    }
    else if (cell !== 0 && cell ==="v") {
      
        if(cell === board[y][x+1] && cell === board[y][x+2] && cell === board[y][x+3] ) {
          console.log("Lig 4 VERTICAL VERMELHA" + (x+1) + ":" + (y+1) + "!");
          
          vit=true;
        }
      }
  }
}

//======= HORIZONTAL ===================================================================
for (let y = 0; y < edgeY; y++) {

    for (let x = 0; x < board[0].length; x++) {
        cell = board[y][x];
 
        if (cell !== 0 && cell ==="p") {
 
            if (cell === board[y+1][x] && cell === board[y+2][x] && cell === board[y+3][x]) {
               console.log("Lig 4 HORIZONTAL PRETA " + (x+1) + ":" + (y+1) + "!")
               
               vit=true;
            }
        }
        else if (cell !== 0 && cell ==="v") {
 
            if (cell === board[y+1][x] && cell === board[y+2][x] && cell === board[y+3][x]) {
               console.log("Lig 4 HORIZONTAL VERMELHA " + (x+1) + ":" + (y+1) + "!")
               
               vit=true;
            }
        }
    }
 }

 //====== DIAGONAL (DIREITA ABAIXO)========================================================
for (let y = 0; y < edgeY; y++) {

    for (let x = 0; x < edgeX; x++) {
        cell = board[y][x];
 
        if (cell !== 0 && cell ==="p") {
 
            if (cell === board[y+1][x+1] && cell === board[y+2][x+2] && cell === board[y+3][x+3]) {
                console.log("Lig 4 Diagonal direita  para baixo PRETA " + (x+1) + ":" + (y+1) + "!")
                
                vit=true;
            }
        }
        else if (cell !== 0 && cell ==="v") {
 
            if (cell === board[y+1][x+1] && cell === board[y+2][x+2] && cell === board[y+3][x+3]) {
                console.log("Lig 4 Diagonal direita para VERMELHA " + (x+1) + ":" + (y+1) + "!")
                
                vit=true;
            }
        }
    }
 }

 //======== DIAGONAL (ESQUERDA ABAIXO) ==========================================================
for (let y = 3; y < board.length; y++) {

    for (let x = 0; x < edgeX; x++) {
        cell = board[y][x];
 
        if (cell !== 0 && cell ==="p") {

            if (cell === board[y-1][x+1] && cell === board[y-2][x+2] && cell === board[y-3][x+3]){
                console.log("Lig 4 Diagonal esquerda para baixo PRETA " + (x+1) + ":" + (y+1) + "!")
                
                vit=true;
            }
        }
        else if (cell !== 0 && cell ==="v") {

            if (cell === board[y-1][x+1] && cell === board[y-2][x+2] && cell === board[y-3][x+3]){
                console.log("Lig 4 Diagonal esquerda para baixo VERMELHA " + (x+1) + ":" + (y+1) + "!")
                
                vit=true;
            }
        }
    }
 }

 return vit;
}

}